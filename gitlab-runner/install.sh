#!/bin/sh

helm upgrade --install --create-namespace --namespace gitlab-runner gitlab-runner -f gitlab-runner.yaml -f gitlab-runner.secret.yaml gitlab/gitlab-runner
helm upgrade --install --namespace gitlab-runner sch-gitlab-runner -f gitlab-runner.yaml -f sch-gitlab-runner.yaml -f sch-gitlab-runner.secret.yaml gitlab/gitlab-runner
